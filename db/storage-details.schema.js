const mongoose = require('mongoose');

const storageDetailsSchema = {
    createdBy: {type: String, required: false},
    createdById: {type: String, required: false},
    updatedBy: {type: String, required: false},
    updatedById: {type: String, required: false},
    deleted: {type: Boolean, default: false}
};

module.exports = storageDetailsSchema;

