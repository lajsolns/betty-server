const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2')

const schema = new mongoose.Schema({
        contact: {
            type: String,
            required: true,
        },
        userIp: {type: String},
        deviceType: {type: String},
        browser: {type: String},
        osVersion: {type: String},
        createdAt: {type: String},
        website: {type: String}

    },
    {timestamps: true});

schema.plugin(mongoosePaginate);
schema.index({contact: 1});

module.exports = mongoose.model('User', schema);






