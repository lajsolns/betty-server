const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require("cors");
const dotenv = require('dotenv');
const helmet = require('helmet');
const logger = require('./utils/logger');
const routes = require('./routes')
const errorhandler = require('errorhandler');
const blueBird = require("bluebird");
const morgan = require('morgan');



global.Promise = blueBird; // set Promise object
mongoose.Promise = blueBird;
dotenv.config(); // load .env variables

const app = express();

app.use(cors());
app.use(helmet());
app.use(morgan('combined', {stream: logger.stream}));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// configure routes
routes(app, '/api/v1/');

// test route
app.get('/', (req, res) => {
    logger.debug('get home');
    res.send('Welcome to betty home');
});

    
// set up error handlers
if (process.env.NODE_ENV === 'development') {
    app.use(errorhandler());
} else {
    app.use((err, req, res, next) => {
        logger.error(err);
        const statusCode = err.statusCode || 500;
        const message = err.errMessage || 'Something went wrong';
        res.status(statusCode).send({message});
    });
}

function startServer() {
    mongoose
        .connect(process.env.DB_URI, {useNewUrlParser: true})
        .then(db => {
            // logger.debug('db connected');
            console.log('db connected');
            app.listen(process.env.PORT, () => {
                // logger.debug(`app running on port ${process.env.PORT}`);
                console.log(`app running on port ${process.env.PORT}`);
            });
        })
        .catch(err => logger.error('error connecting to db', err));
}

startServer();




