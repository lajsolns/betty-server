const {Router} = require('express');

module.exports = (app, apiVersion) => {
    const router = Router();
    app.use(apiVersion, router);
    router.use('/user', require('./user.router'));
    router.use('/winner', require('./winners.router'));
};
