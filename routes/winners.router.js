const {Router} = require('express');
const asyncHandler = require('express-async-handler');
const https = require('https');
const fs = require('fs');
const querystring = require('querystring');
const httpsRequest = require('https-request');

const router = Router();

const postData = querystring.stringify(
    {
            'NetworkID': 34,
            'SkinID': '34002',
            'AggregationLevel': '2',
            "FromDateTime": "/Date(1560764226000)/",
            "ToDateTime": "/Date(1560850626000)/"
    }
);

const options = {
    hostname: 'xgw.888.com',
    port: 446,
    path: '/Bingo.Services/DataService/DataServiceRest.svc/GetWinnerCustomer',
    method: 'POST',
    key: fs.readFileSync('utils/certi.pem'),
    cert: fs.readFileSync('utils/certi.pem'),
    passphrase: 'tree-g^Kn3k{EzXNp',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': Buffer.byteLength(postData)
    }
};

options.agent = new https.Agent(options);

router.get('/winner-details', asyncHandler(async (req, res, next) => {
    console.log('inside winners endpoint');
    const winnerRequest = https.request(options, (res) => {
        console.log(res.statusCode);
        res.on('data', function (data) {
            console.log(`BODY: ${data}`);
        });

        res.on('end', () => {
            console.log('No more data in response.');
        });
    });

    winnerRequest.on('error', (e) => {
        console.log(`problem with request: ${e.message}`);
    });

    winnerRequest.write(postData);
    winnerRequest.end()
}));


module.exports = router;
