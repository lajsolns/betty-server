const {Router} = require('express');
const asyncHandler = require('express-async-handler');

const core = require('../core');

const {userService} = core.services;

const router = Router();

router.post('/create', asyncHandler(async (req, res, next) => {
    const userDetails = req.body.userDetails;
    const _user = await userService.findOneUser(userDetails.contact);
    console.log('user', _user);
    if (_user) {
        res.send({msg: 'User already added'});
    } else {
        const user = await userService.createUser(userDetails);
        res.send({msg: 'User added successfully'});
    }
}));

router.get('/user-details', asyncHandler(async (req, res, next) => {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.pageSize, 10) || 10;
    const users = await userService.findUsersPaginated({page, limit});
    res.send(users);
}));

router.get('/all', asyncHandler(async (req, res, next) => {
    const users = await userService.findAllUsers();
    res.send(users);
}));

module.exports = router;
