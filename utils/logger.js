const winston = require('winston');

const logger = winston.createLogger({
    level: 'debug',
    format: winston.format.combine(
        winston.format.colorize({
            message: true
        }),
        winston.format.simple()
    ),
    transports: [
        new winston.transports.Console()
    ]
});

// stream object for morgan
logger.stream = {
    write: (message, encoding) => {
        logger.info(message);
    }
};


module.exports = logger;