const mongoose = require('mongoose');
const User = require('../../db/models/user.model');
mongoose.model('User');


exports.createUser = (userDetails) => {
    let user = new User(userDetails);
    return user.save();
};

exports.findAllUsers = async () => {
    console.log('in function');
    return await User.find({});
};

exports.findOneUser = async (user) => {
    return await User.findOne({'contact': user});
}

exports.findUsersPaginated = async ({page, limit}) => {
    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const options = {
        page,
        limit,
        customLabels: myCustomLabels,
        sort: {acquired: -1}
    };

    const qry = {};
    return await User.paginate(qry, options);

};
